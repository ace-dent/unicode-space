# Unicode Spiral

A method to draw spirals and square-spirals showing the more-or-less complete set of Unicode characters.

<a href="https://gitlab.com/edent/unicode-space/-/raw/main/Spiral.png"><img src="https://gitlab.com/edent/unicode-space/-/raw/main/Spiral.png" alt="" width="900" /></a>

<a href="https://gitlab.com/edent/unicode-space/-/raw/main/Square.png"><img src="https://gitlab.com/edent/unicode-space/-/raw/main/Square.png" alt="" width="900" /></a>


## Authors and acknowledgement

Created by Terence Eden.

Inspired by:

* [Unicode in a Spiral](https://www.reddit.com/r/typography/comments/3sjq6x/unicode_in_a_spiral/)
* [Hilbert Curve Unicode Poster](https://github.com/hakatashi/unicode-map)

## License

* Source Code: [MIT](https://opensource.org/licenses/MIT)
* Unifont: [GPL2+ & OFL](https://unifoundry.com/LICENSE.txt)
* Images: [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
* Portions of code: [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)

See LICENSE.md for details
