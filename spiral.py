from PIL import Image, ImageDraw, ImageFont
import unicodedata2 as unicodedata
import math

def spiral_points(arc=1, separation=1):
    #   Adapted from https://stackoverflow.com/a/27528612/1127699
    """generate points on an Archimedes' spiral
    with `arc` giving the length of arc between two points
    and `separation` giving the distance between consecutive
    turnings
    - approximate arc length with circle arc at given distance
    - use a spiral equation r = b * phi
    """
    def polar_to_cartesian(r, phi):
        return ( round( r * math.cos(phi) ),
                 round( r * math.sin(phi) )
               )

    # yield a point at origin
    yield (0, 0)

    # initialize the next point in the required distance
    r = arc
    b = separation / (2 * math.pi)
    # find the first phi to satisfy distance of `arc` to the second point
    phi = float(r) / b
    while True:
        yield polar_to_cartesian(r, phi)
        # advance the variables
        # calculate phi that will give desired arc length at current radius
        # (approximating with circle)
        phi += float(arc) / r
        r = b * phi

dimension = 10300

#   Dimension of the image
width  = dimension
height = dimension

#   Image options
background_colour = "white"
font_colour       = "black"

#   Draw image
image = Image.new("1", (width, height), background_colour)
draw = ImageDraw.Draw(image)

#   Generator for points
sp = spiral_points(22, 22)

#   Itterate through all the possible characters
for character_int in range(917999):

    #   Get the next position on the spiral
    x, y = next(sp)

    #   Shift the position to be relative to the centre of the canvas
    x+=(dimension / 2)
    y+=(dimension / 2)

    #   Requires an integer
    x = round(x)
    y = round(y)

    #   Draw initial control characters
    if character_int < 255 :
        character_png = Image.open('pngs/' + str(character_int) + ".png")
        image.paste( character_png, (round(x) , round(y)) )
    else:
        try:
            name = unicodedata.name(chr(character_int))
            character_png = Image.open('pngs/' + str(character_int) + ".png")
            image.paste( character_png, (x , y) )
        except:
            #   Character doesn't have a name - so leave a gap
            pass
            # print( "No character " + str(character_int) )

#   Draw title and copyleft
title_font_name = "unifont-14.0.04.ttf"
title_font = ImageFont.truetype(title_font_name, size=350)

draw.text(
    (100, 100),
    "Unicode 14 Spiral\n℅ @edent\n🄯 CC BY-SA",
    font=title_font,
    fill=font_colour)

#   Save the image
image.save("Spiral.png")
