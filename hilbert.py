from PIL import Image, ImageDraw, ImageFont
import unicodedata2 as unicodedata
from hilbertcurve.hilbertcurve import HilbertCurve
import math

number_of_characters = 173747
character_pixel_size = 17

#   Generator for points
dimensions = 2 #    No cubes or hypercubes!
iterations = round( ( math.log(number_of_characters) / math.log(2) ) / 2 ) #    max length of data must be <= 2^{iterations * dimensions} - 1
hilbert_curve = HilbertCurve(iterations, dimensions)
points = hilbert_curve.points_from_distances( list( range(number_of_characters) ) )

max_points = 2 ** (iterations * dimensions)

dimension = round( math.sqrt( max_points)  * character_pixel_size )

#   Dimension of the image
width  = dimension
height = dimension

#   Image options
background_colour = "white"
font_colour       = "black"

#   Draw image
image = Image.new("1", (width, height), background_colour)
draw = ImageDraw.Draw(image)

#   Placeholders
x_old = 0
y_old = 0
old = False

#   Itterate through all the possible characters
for character_int in range( number_of_characters ):

    #   Get the next position on the curve
    x, y = points[character_int]

    #   Space them out by the width of the character + padding
    x = x * character_pixel_size
    y = y * character_pixel_size

    #   Draw initial control characters
    if character_int < 255 :
        character_png = Image.open('pngs/' + str(character_int) + ".png")
        image.paste( character_png, (round(x) , round(y)) )
    else:
        try:
            name = unicodedata.name(chr(character_int))
            character_png = Image.open('pngs/' + str(character_int) + ".png")
            image.paste( character_png, (x , y) )
            old = True
        except:
            if old:
                pass
            else :
                offset = character_pixel_size / 2
                draw.line([(x_old+offset, y_old+offset), (x+offset, y+offset)], fill=font_colour, width=1, joint=None)
            #   Character doesn't have a name - so leave a gap
            old = False

    x_old = x
    y_old = y

#   Draw title and copyleft
title_font_name = "unifont-14.0.04.ttf"
title_font = ImageFont.truetype(title_font_name, size=600)

draw.text(
    (4600, 150),
    "Unicode 14\nHilbert Curve\n℅ @edent\n🄯 CC BY-SA",
    font=title_font,
    fill=font_colour)

#   Save the image
image.save("Hilbert.png")